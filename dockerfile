# build environment
FROM node:16 as build

WORKDIR /app

COPY package.json ./

RUN npm  install 

COPY . ./

RUN npm run build


# # production environment serve
FROM node:16
COPY --from=build /app/build ./build
RUN npm install -g serve
EXPOSE 3000
CMD [ "serve","-s","build"]
