import { Button, TextField } from "@mui/material";
import styled from "styled-components";

export const Center = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const FormVertical = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;
export const Spacer = styled.div`
  height: ${(props) => props.height ?? "10px"};
  width: ${(props) => props.width ?? "10px"};
`;

export const ButtonV2 = styled(Button)`
  && {
    background: ${(props) => (props.error ? props.bgErrorCol : props.bgCol)};

    &:hover {
      background: ${(props) => props.bgHoverCol};
      transform: scale(1.05);
      transition-duration: 0.4s;
    }
  }
`;
