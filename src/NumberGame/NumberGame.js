import { useState } from "react";
import _ from "lodash";
import { ButtonV2, Center, FormVertical, Spacer } from "./style";
import { TextField } from "@mui/material";

export default function NumberGame({ max = 100, min = 0 }) {
  const [randomNum] = useState(_.floor(Math.random() * (max - min) + min));
  const [guess, setGuess] = useState();
  const [message, setMessage] = useState("");
  const [error, setError] = useState(false);
  const [showResult, setShowResult] = useState(false);
  return (
    <Center>
      <FormVertical
        onSubmit={(event) => {
          if (guess == randomNum) {
            setError(false);

            setMessage("You win! 🏆");
          } else if (guess > randomNum) {
            setError(true);
            setMessage("Try again!");
            setTimeout(() => {
              setMessage("the guess was too hight");
            }, 500);
          } else {
            setMessage("Try again!");
            setError(true);

            setTimeout(() => {
              setMessage("the guess was too low");
            }, 500);
          }
          event.preventDefault();
        }}
      >
        <TextField
          id="outlined-basic"
          label="Your guess"
          variant="outlined"
          type="number"
          error={error}
          value={guess}
          onChange={(event) => {
            setGuess(event.target.value);
          }}
        />
        <Spacer />
        <ButtonV2
          error={error}
          variant="contained"
          type="submit"
          value="Submit"
          bgErrorCol={"#E84A5F"}
          bgCol={"#5d9c59"}
          bgHoverCol={"#21BF73"}
        >
          Check
        </ButtonV2>
      </FormVertical>
      <Spacer />
      <div>{message}</div>
      <Spacer />
      <div hidden={showResult}>{randomNum}</div>
      <ButtonV2
        style={{ position: "absolute", bottom: 20, right: 20 }}
        variant="contained"
        type="submit"
        value="Submit"
        bgCol={"#655DBB"}
        bgHoverCol={"#3E54AC"}
        onClick={() => {
          setShowResult(!showResult);
        }}
      >
        {showResult ? "show result" : "hide result"}
      </ButtonV2>
    </Center>
  );
}
