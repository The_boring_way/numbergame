import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Timer from './Timer';
import NumberGame from './NumberGame/NumberGame';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Timer mm={"00"} ss={"07"} />
    <NumberGame />
  </React.StrictMode>
);

