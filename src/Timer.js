import { useEffect, useState } from "react";
import moment from "moment";
export default function Timer({ mm, ss }) {
  const [timer, setTimer] = useState(
    moment(`2023-01-01T00:${mm}:${ss}+01:00`).format()
  );
  const [stop, setStop] = useState(false);

  useEffect(() => {
    if (!stop) {
      const intervalId = setInterval(() => {
        setTimer(moment(timer).add(-1, "seconds").format());
      }, 1000);
      return () => clearInterval(intervalId);
    }
  });
  useEffect(() => {
    if (!stop)
      setStop(moment().format("00:00") === moment(timer).format("mm:ss"));
  }, [timer]);

  return (
    <div
      style={{
        border: "1px black solid",
        height: "50px",
        width: "150px",
      }}
    >
      <div>{moment(timer).format("mm:ss")}</div>
      {stop ? <div>Timer equal to 0</div> : <></>}
    </div>
  );
}
